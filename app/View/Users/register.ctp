<div class="card shadow" style="width: 27rem;">
    <div class="card-body">
        <h5 class="card-title">Register User</h5>
        <?php
        echo $this->Flash->render();
        echo $this->Form->create('User', [
            'url' => ['controller' => 'users', 'action' => 'register'],
            'id' => 'UsersRegister'
        ]);
        echo $this->Form->input(
            'username',
            [
                'label' => 'Username:',
                'class' => 'form-control-custom',
                'type' => 'text'
            ]
        );
        echo $this->Form->input(
            'password',
            [
                'label' => 'Password:',
                'class' => 'form-control-custom',
                'type' => 'password'
            ]
        );
        echo $this->Form->input(
            'confirm_password',
            [
                'label' => 'Confirm Password:',
                'class' => 'form-control-custom',
                'type' => 'password'
            ]
        );
        echo $this->Form->input(
            'email',
            [
                'label' => 'Email:',
                'class' => 'form-control-custom',
                'type' => 'email'
            ]
        );
        echo $this->Form->end(
            [
                'label' => 'Register',
                'class' => 'form-control-custom success-custom',
            ]
        );

        ?>
    </div>
</div>