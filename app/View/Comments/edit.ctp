<?php
if (isset($error)) {
    if ($error != '') {
        echo '
            <div class="alert alert-danger alert-dismissible fade show" role="alert">' .
                $error . '
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>   
            </div>';
    }
}
?>
<?php echo $this->Form->create('Comment', [
    'url' => ['controller' => 'comments', 'action' => 'editPost'],
]);
?>
<?php echo $this->Form->input(
    'comment',
    [
        'label' => false,
        'class' => 'form-control',
        'type' => 'textarea',
        'id' => 'input-post'
    ]
);
?>
<div class='float-right'>
    <div class='input-group' id='button_post_div'>
        <input type='text' id='input-post-count' disabled size='4' />
        <?php
        echo '&nbsp';
        echo $this->Form->end([
            'label' => 'Comment',
            'class' => 'btn-sm btn-success pull-right',
        ]);
        ?>
    </div>
</div>