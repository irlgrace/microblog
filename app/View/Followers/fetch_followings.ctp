<ul class="list-group list-group-flush">
    <?php
    foreach ($followings as $following) {
        echo '<li class="list-group-item">' .
            '<div class="div_pic_name">' .
            $this->Html->image(
                empty($following['FollowedUser']['image']) ? 'user.jpg' : h($following['FollowedUser']['image']),
                [
                    'alt' => 'You',
                    'style' => 'border-radius:50%; width: 30px;'
                ]
            ) .
            '&nbsp;' .
            '<h5>' . $this->Html->link(
                h($following['FollowedUser']['username']),
                ['controller' => 'users', 'action' => 'userPage', h($following['FollowedUser']['id'])],
            ) . '</h5>' .
            '&nbsp;' .
            '<small><em>' . h($following['FollowedUser']['email']) . '</em></small>' .
            '</div>' .
            '</li>';
    }
    ?>
</ul>