<?php
    if(isset($error)) {
        if ($error != '') {
            echo '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">' .
                $error . '
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        }
    }
?>
<?php echo $this->Form->create('Post',[
            'url' => array('controller' => 'posts', 'action' => 'editPost'),
            'id' => 'PostsEdit',
        ]); 
?> 
<?php echo $this->Form->input('post',[
            'label' => false,
            'id' => 'input-post',
            'class' => 'form-control',
            'type' => 'textarea',
        ]); 
?>
<div class="float-right">
    <div class="input-group" id="button_post_div">
        <input type="text" id="input-post-count" disabled size="4"/>
        &nbsp;
        <?php echo $this->Form->end([
                    'label' => 'Post',
                    'class' => 'btn-sm btn-success',
                ]); 
        ?> 
    </div>
</div>