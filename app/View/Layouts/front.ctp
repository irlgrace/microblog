<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $title; ?></title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<!-- Include external files and scripts here (See HTML helper for more info.) -->
<?php
    echo $this->Html->css(array('bootstrap.min', 'front_design'));
    echo $this->Html->script(array('jquery-3.3.1.slim.min', 'popper.min', 'bootstrap.min'));

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
?>
</head>
<body>
<?php 
    echo $this->element('outside_header');
?>
<div class='container'>
    <br/>
    <div id='content' class='row'>
        <div class='col-md-6'>
            <div class='content_div' id='div_image'>
                <?php echo $this->Html->image('front_background.jpg', array('alt' => 'Microblog2'));?>
            </div>
        </div>
        <div class='col-md-6'>
            <div class='float-right'>
                <?php 
                    echo $this->fetch('content');
                ?>
            </div>
        </div>
    </div>
    <br/>
</div>
<div id='footer'>

</div>
</body>
</html>