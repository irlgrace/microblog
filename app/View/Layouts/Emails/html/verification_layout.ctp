<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php echo $this->fetch('title'); ?></title>
</head>
<body style="font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;">
    <h3>Hi <b><?php echo $name; ?></b>!</h3>
    <br/>
    <p>Welcome to Microblog 2! You just created an account in our site. Please Activate the account.</p>
    <br/>
    Click <a href='<?php echo $verification_link; ?>' target='_blank'>here</a> to activate the account.
    <br/>
    <br/>
    <h1><b><?php echo $code; ?></b></h1>
    To Manually verify the account using username and activation code, click <a href='<?php echo $link; ?>' target='_blank'>here</a>
    <br/>
    <?php echo $this->fetch('content'); ?>
    <br/>
    <p>Thank you.</p>
</body>
</html>