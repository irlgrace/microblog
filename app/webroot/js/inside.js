var inputPost = document.querySelector("#input-post");
var maxCharLimit = 140;

if (inputPost != null) {
    document.querySelector("#input-post-count").value = inputPost.value.length + '/' + maxCharLimit;
    inputPost.addEventListener("keyup", function (e) {
        var lengthCount = this.value.length;
        if (lengthCount == maxCharLimit) {
            e.preventDefault();
        }
        document.querySelector("#input-post-count").value = lengthCount + '/' + maxCharLimit;
    });
}



//To determine if there is scrollable pagination in the page
var loadDataElement = document.querySelector('#load_data');

if (loadDataElement != null) {

    lazzyLoader();

    if (action() == 'inactive') {
        document.querySelector('#action_scroll').value = 'active';
        loadData(data(limit, page), url);
    }

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() > $("#load_data").height() && action() == 'inactive') {
            lazzyLoader();
            document.querySelector('#action_scroll').value = 'active';
            page = page + 1;
            setTimeout(function () {
                loadData(data(limit, page), url);
            }, 1000);
        }
    });

}


