<?php
App::uses('AppModel', 'Model');
class Comment extends AppModel
{
    public $actsAs = ['Containable'];

    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'counterCache' => true,
            'counterScope' => [
                'Comment.deleted' => false
            ]
        ]
    ];

    public $validate = [
        'comment' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Ops. You forgot to say something.'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 140],
                'message' => 'Comment must be not more than 140 characters.'
            ]
        ]
    ];

    /**
     * Check if comment is own by user
     *  
     * @return bool 
     * @param $comment, $user
     */
    public function isOwnedBy($comment, $user)
    {
        return $this->field('id', array('id' => $comment, 'user_id' => $user)) !== false;
    }

    /**
     * Check if comment is exiting and not deleted 
     *  
     * @return bool 
     * @param $comment
     */
    public function isInvalid($comment)
    {
        if ($this->hasAny(['Comment.id' => $comment])) {
            $deleted = $this->read('deleted', $comment);
            return $deleted['Comment']['deleted'];
        }
        return true;
    }

    /**
     * Function for Retrieving Comment for Specific Post 
     *  
     * @return $comments array 
     * @param $post_id, $limit, $page
     */
    public function fetchComment($post_id, $limit, $page)
    {
        $comments = $this->find(
            'all',
            [
                'conditions' => [
                    'Comment.deleted' => false,
                    'Comment.post_id' => $post_id
                ],
                'order' => ['Comment.created' => 'asc'],
                'limit' => $limit,
                'page' => $page,
                'contain' => [
                    'User.username',
                    'User.image',
                ],
            ]
        );

        return $comments;
    }
}
