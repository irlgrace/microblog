<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class User extends AppModel
{
    public $actsAs = ['Containable'];
    public $hasMany = [
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'user_id',
            'conditions' => ['Post.deleted' => false],
            'order' => 'Post.created DESC',
            'dependent' => true
        ],
        'Comment' => [
            'className' => 'Comment',
            'foreignKey' => 'user_id',
            'conditions' => ['Comment.deleted' => false],
            'order' => 'Comment.created DESC',
            'dependent' => true
        ],
        'Like' => [
            'className' => 'Like',
            'foreignKey' => 'user_id',
            'conditions' => ['Like.deleted' => false],
            'dependent' => true
        ],
        'Follower' => [
            'className' => 'Follower',
            'foreignKey' => 'following_user_id',
            'conditions' => ['Follower.deleted' => false],
            'order' => 'Follower.created DESC',
            'limit' => 5,
        ],
        'Following' => [
            'className' => 'Follower',
            'foreignKey' => 'user_id',
            'conditions' => ['Following.deleted' => false],
            'order' => 'Following.created DESC',
            'limit' => 5,
        ]
    ];
    public $validate = [
        'username' =>  [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'A username is required.'
            ],
            'alphaNumeric' => [
                'rule' => 'alphaNumericDashUnderscore',
                'message' => 'Underscore, letters and numbers only'
            ],
            'checkUnique' => [
                'rule' => 'isUnique',
                'message' => 'Username already taken.'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 50],
                'message' => 'Username must not be more than 50 characters long.'
            ]
        ],
        'password' =>  [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Password is required.'
            ],
            'minLength' => [
                'rule' => ['minLength', 8],
                'message' => 'Password should be at least 8 characters long.'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 225],
                'message' => 'Password must not be more than 225 characters long.'
            ]
        ],
        'confirm_password' => [
            'rule' => 'confirmPassword',
            'message' => 'Password mismatch.'
        ],
        'email' => [
            'rule' => 'email',
            'message' => 'Invalid Email'
        ],
        'activation_code' => [
            'rule' => 'notBlank',
            'message' => 'Activation code is required.'
        ],
    ];

    /**
     * Before saving Hash the password 
     *  
     * @return bool 
     * @param $options array
     */
    public function beforeSave($options = [])
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    /**
     * Checking if Field send is alphanumeric 
     *  
     * @return bool 
     * @param $check
     */
    public function alphaNumericDashUnderscore($check)
    {
        $value = array_values($check);
        $value = $value[0];

        return preg_match('|^[0-9a-zA-Z_]*$|', $value);
    }

    /**
     * Checking if Confirm Password and Password is the same 
     *  
     * @return bool 
     */
    public function confirmPassword()
    {
        return $this->data['User']['password'] == $this->data['User']['confirm_password'];
    }

    /**
     * Function for searching user 
     *  
     * @return $users array 
     * @param $word $limit $page
     */
    public function searchUser($word, $limit, $page)
    {
        $users = $this->find(
            'all',
            [
                'limit' => $limit,
                'page' => $page,
                'conditions' => [
                    'User.deleted' => false,
                    'User.activated' => true,    
                    'OR' => [
                        'User.username LIKE' => '%' . $word . '%',
                        'User.email LIKE' => '%' . $word . '%'
                    ]
                ],
                'contain' => false,
            ],
        );

        return $users;
    }

    /**
     * Check if User is existing and not deleted 
     *  
     * @return bool 
     * @param $user 
     */
    public function isInvalid($user)
    {
        if ($this->hasAny(['User.id' => $user])) {
            $deleted = $this->read('deleted', $user);
            $activated = $this->read('activated', $user);

            return $deleted['User']['deleted'] || (!$activated['User']['activated']);
        }
        return true;
    }
}
